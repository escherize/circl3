function love.conf(t)
    t.version = "0.10.2"
    t.window.width = 1024 * 1.5
    t.window.height = 768 * 1.1
    math.randomseed(os.time())
end
