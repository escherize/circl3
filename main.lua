w = love.graphics.getWidth()
h = love.graphics.getHeight()
love.graphics.setBackgroundColor(10, 10, 10)

invincibilityFrames = 0.5
step = 1.0
itboost = 1.4
friction = 0.95
radius = 25
radiusSquared = radius * radius * 4
initialHp = 10
hpBonus = 4
gameOver = false
winner = nil

hitSound = love.audio.newSource("slap.mp3", "static")
dieSound = love.audio.newSource("metroid.mp3", "static")
winSound = love.audio.newSource("alien.mp3", "static")

function createPlayer(l, r, u, d, color)
    local player = {}
    player.color = color or 0
    player.l = l
    player.r = r
    player.u = u
    player.d = d
    return player
end

initialPlayers = {
    createPlayer("a", "d", "w", "s")
  , createPlayer("left", "right", "up", "down", 85)
  , createPlayer("j", "l", "i", "k", 2 * 85) 
}


function reset()
    love.audio.stop(winSound)
    gameOver = false
    players = {}
    for i = 1, #initialPlayers do
        player = initialPlayers[i]
        player.hp = initialHp
        player.invincibility = 0
        player.dx = 0
        player.dy = 0
        player.x = math.random() * w
        player.y = math.random() * h
        table.insert(players, player)
    end
    if winner then 
        itplayer = winner 
    else
        itplayer = players[math.random(#players)]
    end
    itplayer.hp = itplayer.hp + hpBonus
end

reset()

function dist_sq (x1, y1, x2, y2)
    dx1 = math.abs( x1 - x2 )
    dy1 = math.abs( y1 - y2 )
    dx2 = w - dx1
    dy2 = h - dy1
    dx = math.min(dx1, dx2)
    dy = math.min(dy1, dy2)
    return dx*dx + dy*dy
end

function collide (x1, y1, x2, y2)
    return dist_sq (x1, y1, x2, y2) <= radiusSquared
end

function love.keypressed(key)
    if key == "space" and gameOver then
        reset()
    end
end

function love.update(dt)
    if gameOver then return end
    -- We will increase the variable by 1 for every second the key is held down.
    deadguyidx = nil
    for i = 1, #players do
        guy = players[i]
        if guy.hp <= 0 then
            deadguyidx = i
        end
        guystep = step * (guy == itplayer and itboost or 1)
        if love.keyboard.isDown(guy.r) then guy.dx = guy.dx+guystep end
        if love.keyboard.isDown(guy.l) then guy.dx = guy.dx-guystep end
        if love.keyboard.isDown(guy.d) then guy.dy = guy.dy+guystep end
        if love.keyboard.isDown(guy.u) then guy.dy = guy.dy-guystep end

        if guy.invincibility > 0 then
            guy.invincibility = math.max(0, guy.invincibility - dt)
        end

        guy.x = guy.x + guy.dx
        guy.y = guy.y + guy.dy
        guy.dx = guy.dx * friction
        guy.dy = guy.dy * friction
        guy.x = guy.x % w
        guy.y = guy.y % h
    end

    if deadguyidx then
        table.remove(players, deadguyidx)
        newit = {}
        newit.hp = -1

        for i = 1, #players, 1 do
            guy = players[i]
            if guy.hp > newit.hp then newit = guy end
        end
        itplayer = newit
    end

    if #players == 1 then
        gameOver = true
        love.audio.stop(winSound)
        love.audio.play(winSound)
        return
    else
        if deadguyidx then
            love.audio.stop(dieSound)
            love.audio.play(dieSound)
        end
    end

    tagOccured = false
    for i = 1, #players, 1 do
        guy = players[i]

        if guy ~= itplayer then
            guy.hp = guy.hp + (dt / #players)
        end

        if not tagOccured and
           guy.invincibility == 0 and
           guy ~= itplayer and
           collide(guy.x, guy.y, itplayer.x, itplayer.y) then
                love.audio.stop(hitSound)
                love.audio.play(hitSound)
                tagOccured = true
                itplayer.invincibility = invincibilityFrames
                itplayer = guy
        end
    end
    itplayer.hp = itplayer.hp - dt
end

function love.draw()
    function HSL(h, s, l, a)
        if s<=0 then return l,l,l,a end
        h, s, l = h/256*6, s/255, l/255
        local c = (1-math.abs(2*l-1))*s
        local x = (1-math.abs(h%2-1))*c
        local m,r,g,b = (l-.5*c), 0,0,0
        if h < 1     then r,g,b = c,x,0
        elseif h < 2 then r,g,b = x,c,0
        elseif h < 3 then r,g,b = 0,c,x
        elseif h < 4 then r,g,b = 0,x,c
        elseif h < 5 then r,g,b = x,0,c
        else              r,g,b = c,0,x
        end return (r+m)*255,(g+m)*255,(b+m)*255,a
    end

    for i = 1, #players, 1 do
        guy = players[i]
        love.graphics.setColor(HSL(guy.color, 255, 100))
        -- see yourself on both sides of the taurus
        love.graphics.circle("fill", guy.x, guy.y, radius)
        love.graphics.circle("fill", guy.x + w, guy.y, radius)
        love.graphics.circle("fill", guy.x - w, guy.y, radius)
        love.graphics.circle("fill", guy.x, guy.y + h, radius)
        love.graphics.circle("fill", guy.x + w, guy.y + h, radius)
        love.graphics.circle("fill", guy.x - w, guy.y + h, radius)
        love.graphics.circle("fill", guy.x, guy.y - h, radius)
        love.graphics.circle("fill", guy.x + w, guy.y - h, radius)
        love.graphics.circle("fill", guy.x - w, guy.y - h, radius)

            if guy.invincibility == 0 then
                bigRad = math.min(h, w) / 4 - radius
                outerRad = guy.hp/initialHp * bigRad + radius
                love.graphics.circle("line", guy.x, guy.y, outerRad)
                love.graphics.circle("line", guy.x + w, guy.y, outerRad)
                love.graphics.circle("line", guy.x - w, guy.y, outerRad)
                love.graphics.circle("line", guy.x, guy.y + h, outerRad)
                love.graphics.circle("line", guy.x + w, guy.y + h, outerRad)
                love.graphics.circle("line", guy.x - w, guy.y + h, outerRad)
                love.graphics.circle("line", guy.x, guy.y - h, outerRad)
                love.graphics.circle("line", guy.x + w, guy.y - h, outerRad)
                love.graphics.circle("line", guy.x - w, guy.y - h, outerRad)
                love.graphics.setColor(HSL(guy.color, 128, 200, 20))
                love.graphics.circle("fill", guy.x, guy.y, outerRad)
                love.graphics.circle("fill", guy.x + w, guy.y, outerRad)
                love.graphics.circle("fill", guy.x - w, guy.y, outerRad)
                love.graphics.circle("fill", guy.x, guy.y + h, outerRad)
                love.graphics.circle("fill", guy.x + w, guy.y + h, outerRad)
                love.graphics.circle("fill", guy.x - w, guy.y + h, outerRad)
                love.graphics.circle("fill", guy.x, guy.y - h, outerRad)
                love.graphics.circle("fill", guy.x + w, guy.y - h, outerRad)
                love.graphics.circle("fill", guy.x - w, guy.y - h, outerRad)
        end
        if guy == itplayer then
            inRad = radius / 2
            love.graphics.setColor(HSL(guy.color, 128, 0))
            love.graphics.circle("fill", guy.x, guy.y, inRad)
            love.graphics.circle("fill", guy.x + w, guy.y, inRad)
            love.graphics.circle("fill", guy.x - w, guy.y, inRad)
            love.graphics.circle("fill", guy.x, guy.y + h, inRad)
            love.graphics.circle("fill", guy.x + w, guy.y + h, inRad)
            love.graphics.circle("fill", guy.x - w, guy.y + h, inRad)
            love.graphics.circle("fill", guy.x, guy.y - h, inRad)
            love.graphics.circle("fill", guy.x + w, guy.y - h, inRad)
            love.graphics.circle("fill", guy.x - w, guy.y - h, inRad)
        end
    end
    if gameOver == true then
        winner = players[1]
        love.graphics.setColor(HSL(winner.color, 256, 128))
        love.graphics.print("Game over!\nPress space.", w/2 - 200, h/2 - 200, 0, 10, 10)
    end
end
